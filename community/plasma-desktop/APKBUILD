# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=plasma-desktop
pkgver=5.23.1
pkgrel=0
pkgdesc="KDE Plasma Desktop"
# armhf blocked by qt5-qtdeclarative
# s390x and mips64 blocked by ibus
# ppc64le blocked by kaccounts-integration
# riscv64 blocked by polkit -> kwin
arch="all !armhf !s390x !mips64 !ppc64le !riscv64"
url='https://kde.org/plasma-desktop/'
license="GPL-2.0-only AND LGPL-2.1-only"
depends="
	accountsservice
	font-noto-emoji
	ibus-emoji
	kirigami2
	plasma-workspace
	qqc2-desktop-style
	setxkbmap
	"
makedepends="
	attica-dev
	baloo-dev
	eudev-dev
	extra-cmake-modules
	fontconfig-dev
	ibus-dev
	kaccounts-integration-dev
	kactivities-dev
	kactivities-stats-dev
	kauth-dev
	kcmutils-dev
	kcodecs-dev
	kconfig-dev
	kdbusaddons-dev
	kdeclarative-dev
	kdelibs4support-dev
	kdoctools-dev
	kemoticons-dev
	kglobalaccel-dev
	ki18n-dev
	kitemmodels-dev
	knewstuff-dev
	knotifications-dev
	knotifyconfig-dev
	kpeople-dev
	krunner-dev
	kwallet-dev
	kwin-dev
	libxcursor-dev
	libxi-dev
	libxkbfile-dev
	plasma-framework-dev
	plasma-workspace-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtsvg-dev
	qt5-qtx11extras-dev
	xf86-input-evdev-dev
	xf86-input-libinput-dev
	xf86-input-synaptics-dev
	"
checkdepends="xvfb-run iso-codes"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/plasma-desktop-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang knetattach"
options="!check" # Requires running dbus

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build

	rm "$pkgdir"/usr/bin/knetattach
	rm "$pkgdir"/usr/share/applications/org.kde.knetattach.desktop
}

knetattach() {
	pkgdesc="Wizard which makes it easier to integrate network resources with the Plasma Desktop"
	depends="kdelibs4support"

	cd "$builddir"/build/knetattach
	DESTDIR="$subpkgdir" make install
}
sha512sums="
f6f0483e9dc8e64b4def5e723d259ea32ab5b1556d8486a406b3c65e55d5b7d332614ded8c4f651785890067cd6920a497a110905f2cedb9383b254eab8b2505  plasma-desktop-5.23.1.tar.xz
"
