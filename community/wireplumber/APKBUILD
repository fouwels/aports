# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=wireplumber
pkgver=0.4.4
pkgrel=1
pkgdesc="Session / policy manager implementation for PipeWire"
url="https://pipewire.org/"
# s390x blocked by pipewire
arch="all !s390x"
license="LGPL-2.1-or-later"
makedepends="
	doxygen
	glib-dev
	graphviz
	lua5.4-dev
	meson
	pipewire-dev>=0.3.39
	"
checkdepends="
	dbus
	pipewire
	"
subpackages="$pkgname-dev"
source="https://gitlab.freedesktop.org/PipeWire/wireplumber/-/archive/$pkgver/wireplumber-$pkgver.tar.gz"

case "$CARCH" in
	armhf|armv7) options="$options !check" # Test failures https://gitlab.freedesktop.org/pipewire/wireplumber/-/issues/81
esac

provides="pipewire-session-manager"

build() {
	abuild-meson \
		-Dsystem-lua=true \
		. output
	meson compile ${JOBS:+-j ${JOBS}} -C output
}

check() {
	meson test --no-rebuild -v -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}
sha512sums="
f8d764c1728c2e550fc41044a8ebbbeba27157540077648431796d7de5a04281df00d9742b04c6f76b50e446b2a723f2555520cb953d432f90b05148967de4ef  wireplumber-0.4.4.tar.gz
"
